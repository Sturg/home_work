let url = 'https://reqres.in/api/users?per_page=12';


console.log('-----------');
console.log('Пункт №1:')
console.log('-----------');

let data = fetch(url)
  .then((response) => response.json())
  .then(({data}) => {
    console.log(data)
    return data;
  })

data.then(data => {
  console.log('-----------');
  console.log('Пункт №2:')
  console.log('-----------');
  data.forEach((item) => {

    console.log(item.last_name)
  })

});


data.then(data => {
  console.log('-----------');
  console.log('Пункт №3:')
  console.log('-----------');
  let resultSearch = data.filter((item) => {
    return item?.last_name[0] === 'F'
  })
  resultSearch.forEach((search) => {
    console.log(search.last_name)
  })
})


data.then(data => {
  console.log('-----------');
  console.log('Пункт №4:')
  console.log('-----------');
  const newArray = data.map(item => {
    const array = {};
                                                                //Блин чет дико усложнил)))) ну да ладно) Конечно можно было не мапить)
    array.name = item.first_name;
    array.lastName = item.last_name;
    return array;
  })

    const array3 = newArray.reduce((accum, item) =>{
      accum.push(item.name + ' ' + item.lastName)
      return accum;
    },[])
  let showArray = array3.join(', ')
  console.log('Наша база содержит данные следующих пользователей:' + ' ' + showArray)
})

data.then(data =>{
  console.log('-----------');
  console.log('Пункт №5:')
  console.log('-----------');
  for (const key in data){
    console.log(key)
  }
})



